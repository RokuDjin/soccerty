$(function(){
    $("#tooltip-enabled, #max-length").tooltip();

    $(".chzn-select").each(function(){
        $(this).select2($(this).data());
    });

    $('.selectpicker').selectpicker();
    $('.selectpicker + .bootstrap-select span.caret').replaceWith("<i class='fa fa-caret-down'></i>");
    $('.selectpicker + .bootstrap-select span.pull-left').removeClass("pull-left");

    $('.date-picker').datepicker({
        autoclose: true
    });
    var $btnCalendar = $('#btn-select-calendar');
    $btnCalendar.datepicker({
        autoclose: true
    }).on('changeDate', function(ev){
            $('#btn-enabled-date').val($btnCalendar.data('date'));
        $btnCalendar.datepicker('hide');
    });

    $("#mask-phone").mask("(999) 999-9999");
    $("#mask-date").mask("99-99-9999");
    $("#mask-int-phone").mask("+999 999 999 999");
    $("#mask-time").mask("99:99");
});