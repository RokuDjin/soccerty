class Mailer < ActionMailer::Base
  default from: "soccerty@gmail.com"

  def invitation(email, captain, team)
  	@captain = captain
  	@team = team
  	mail(to: email, subject: 'Soccery Team Invitation')
  end
end
