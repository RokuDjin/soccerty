module ApplicationHelper
	include RoleHelper

	def title(page_title)
	  content_for(:title) { page_title }
	end

	def page_category(page_category)
	  content_for(:page_category) { page_category }
	end

  def message_type(message_type)
    content_for(:message_type) { message_type }
  end
end
