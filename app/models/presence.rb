class Presence < ActiveRecord::Base
	belongs_to :game
	belongs_to :user
	belongs_to :winner, class_name: 'Team'
end
