class City < ActiveRecord::Base
  has_many :soccer_fields
  has_many :users
end
