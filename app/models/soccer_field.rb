class SoccerField < ActiveRecord::Base

  has_one :serie
  has_one :game
  belongs_to :city

	validates :name, presence: true
	validates :address, presence: true
	validates :city, presence: true
	validates :postal, presence: true
	validates :category, presence: true

  def full_address
    [
      self.address,
      self.city.name,
      self.city.state,
      self.city.country,
      self.postal,
    ].reject { |s| s.blank? }.join(", ")
  end

end
