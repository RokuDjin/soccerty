class TeamChat < ActiveRecord::Base
	belongs_to :team
  belongs_to :user

  def self.new_game_chat(team_id, user_id, message)
    create(team_id: team_id, user_id: user_id, time: Time.now, message: message)
    Feed.new_game_chat(team_id, user_id, message)
  end

end
