class Serie < ActiveRecord::Base
  include RoleHelper
	
  has_many :games, dependent: :destroy
  has_many :teams
  belongs_to :soccer_field

  accepts_nested_attributes_for :soccer_field

  validates :game_type, presence: true, :numericality => { greater_than_or_equal_to: 4, less_than_or_equal_to: 11 }
  validates :game_category, presence: true, :numericality => { greater_than_or_equal_to: 0, less_than_or_equal_to: 3 }
  validates :level, presence: true, :numericality => { greater_than_or_equal_to: 0, less_than_or_equal_to: 2 }
  validates :number_games, presence: true, :numericality => { greater_than_or_equal_to: 1, less_than_or_equal_to: 100 }
  validates :soccer_field, presence: true
  validates_presence_of :start, :message => "must be valid"
  validates :duration, presence: true, :numericality => { greater_than_or_equal_to: 15, less_than: 500 }
  validates :recurring, presence: true, :numericality => { greater_than_or_equal_to: 0, less_than_or_equal_to: 3 }
  validates :status, presence: true, :numericality => { greater_than_or_equal_to: 0, less_than_or_equal_to: 1 }

  after_create {setup}

	def setup
    # Create individual team and placeholders
    teams << Team.create(name: "Team #1", color: Team::COLORS.sample, team_type: 3,serie: self)
    for i in 2..6
      teams << Team.create(name: "Team ##{i}", color: Team::COLORS.sample, serie: self)
    end
    # Week 1
    games << Game.new(number: 1, home: teams[0], away: teams[1], time: (self.time+(week*7*24*60*60)), duration: self.duration, soccer_field_id: self.soccer_field_id)
    games << Game.new(number: 2, home: teams[2], away: teams[3], time: (self.time+self.duration+(week*7*24*60*60)), duration: self.duration, soccer_field_id: self.soccer_field_id)
    games << Game.new(number: 3, home: teams[4], away: teams[5], time: (self.time+2*self.duration+(week*7*24*60*60)), duration: self.duration, soccer_field_id: self.soccer_field_id)
    # Week 2
    games << Game.new(number: 4, home: teams[1], away: teams[2], time: (self.time+(week*7*24*60*60)), duration: self.duration, soccer_field_id: self.soccer_field_id)
    games << Game.new(number: 5, home: teams[3], away: teams[4], time: (self.time+self.duration+(week*7*24*60*60)), duration: self.duration, soccer_field_id: self.soccer_field_id)
    games << Game.new(number: 6, home: teams[5], away: teams[0], time: (self.time+2*self.duration+(week*7*24*60*60)), duration: self.duration, soccer_field_id: self.soccer_field_id)
    # Week 3
    games << Game.new(number: 7, home: teams[2], away: teams[4], time: (self.time+(week*7*24*60*60)), duration: self.duration, soccer_field_id: self.soccer_field_id)
    games << Game.new(number: 8, home: teams[0], away: teams[3], time: (self.time+self.duration+(week*7*24*60*60)), duration: self.duration, soccer_field_id: self.soccer_field_id)
    games << Game.new(number: 9, home: teams[5], away: teams[1], time: (self.time+2*self.duration+(week*7*24*60*60)), duration: self.duration, soccer_field_id: self.soccer_field_id)
    # Week 4
    games << Game.new(number: 10, home: teams[0], away: teams[2], time: (self.time+(week*7*24*60*60)), duration: self.duration, soccer_field_id: self.soccer_field_id)
    games << Game.new(number: 11, home: teams[1], away: teams[3], time: (self.time+self.duration+(week*7*24*60*60)), duration: self.duration, soccer_field_id: self.soccer_field_id)
    games << Game.new(number: 12, home: teams[2], away: teams[0], time: (self.time+2*self.duration+(week*7*24*60*60)), duration: self.duration, soccer_field_id: self.soccer_field_id)
    # Week 5
    games << Game.new(number: 13, home: teams[0], away: teams[5], time: (self.time+(week*7*24*60*60)), duration: self.duration, soccer_field_id: self.soccer_field_id)
    games << Game.new(number: 14, home: teams[1], away: teams[2], time: (self.time+self.duration+(week*7*24*60*60)), duration: self.duration, soccer_field_id: self.soccer_field_id)
    games << Game.new(number: 15, home: teams[1], away: teams[0], time: (self.time+2*self.duration+(week*7*24*60*60)), duration: self.duration, soccer_field_id: self.soccer_field_id)

    save
	end

	def retrieve_games
		games = []
		for i in 0..self.number_games-1
			game_time = RoleHelper.serie_next_recurring(self.start, self.recurring, i)
			games << {id: i, title: "game ##{i+1}", start: game_time}
		end
		games
	end

end
