class Team < ActiveRecord::Base
  has_and_belongs_to_many :users
  belongs_to :serie
  belongs_to :captain, class_name: 'User'
  has_many :team_chats
  has_many :games
  has_many :invitations

  NAMES = ["Aero Bandits", "Cunning Coyotes", "Hattrick Heroes", "Own Goal", "Scouting For Goals", "Wise Monkeys", "Red Busters", "Tornado Kickers", "Sharpshooters", "Tornado Geckos", "Wild Chasers", "Silent Rebels", "Flash Mutants", 
  "Dark Commandos", "Shaolin Champions", "Sneaky Gangstaz", "Muffin Ninjas", "Shooting Bandits", "Rockin Fighters", "Fighting Bullets", "Militant Fireballs", "Scorpion Tigers", "Carnivore Mashers", "Hot Crunchers", "Night Heroes", 
  "Wind Kickers", "Flying Sharks", "Blue Legends", "Delta Chasers", "Striking Sharks"]

  COLORS = ["White", "Black", "Red", "Blue"]

  def wins
  	Game.where(winner: self).count
  end

  def losses
  	Game.where("home_id = #{self.id} OR away_id = #{self.id}").count - Game.where(winner: self).count
  end

  def points
  	Game.where(winner: self).count*3 + Game.where(winner_id: 0).where("home_id = #{self.id} OR away_id = #{self.id}").count 
  end

end
