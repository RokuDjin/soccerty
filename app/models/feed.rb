class Feed < ActiveRecord::Base
  belongs_to :user

  def self.new_game_chat(team_id, user_id, message)
    user = User.find(user_id)
    team = Game.find(team_id)
    team.users.each do |player|
      create(user_id: player.id, content: "New message from <a href='/users/#{user.id}'>#{user.first_name}</a>: <em>#{message}</em>", feed_type: 1)
    end
  end

  def self.joined_serie(user_id, serie)
    self.create(user_id: user_id, content: "Congratulations! You are now part of <a href='/series/#{serie.id}'>Serie ##{serie.id}</a>", feed_type: 2)
  end

  def self.joined_team(teams)
    teams.each do |team|
      team.participations.each do |participation|
        self.create(user_id: participation.user_id, content: "Hoorah! We placed you in team <em>#{team.name}</em> playing in <em>#{team.color}</em> for your next <a href='/series/#{participation.serie.id}'>games(s)</a>", feed_type: 3)
      end
    end
  end

  def self.team_invitation(team, player)
    self.create(user_id: player.id, content: "You received an invitation to join <em><a href='/teams/#{team.id}'>#{team.name}</a></em>. Click <a href='/teams/accept?id=#{team.id}'>here</a> to accept the invitation.", feed_type: 3)
  end

  def self.cannot_make_it(user_id, game_id, team)
    player = User.find(user_id)
    game = Game.find(game_id)
    game_time = game.time.strftime('%b')+' '+game.time.day.ordinalize
    game.presences.where(team: team).each do |presence|
      self.create(user_id: presence.user.id, content: "<a href='/users/#{player.id}'>#{player.first_name}</a> cannot make it to <a href='/games/#{game.id}'>#{game_time} game</a>", feed_type: 4)
    end
  end

  def self.game_results(game)
    game.presences.each do |presence|
      self.create(user_id: presence.user_id, content: "The <a href='/games/#{game.id}'>game</a> you played on #{game.time.strftime('%b')+' '+game.time.day.ordinalize} results are up", feed_type: 2)
    end
  end

  def self.welcome(user)
    self.create(user_id: user.id, content: "Welcome #{user.first_name} to Soccerty!", feed_type: 0)
    Invitation.where(email: user.email).each do |invitation|
      self.team_invitation(invitation.team, user)
    end
  end

end
