class Invitation < ActiveRecord::Base
	belongs_to :team

	validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, message: "please enter a valid email address"

	after_create {process}

	def process
		player = User.find_by_email(self.email)
		if player.nil?
			Mailer.invitation(self.email, self.team.captain, self.team).deliver
		else
			Feed.team_invitation(self.team, player)
		end
	end

end