module RoleHelper

	def self.feed_type(feed_type)
    if feed_type == 0
      "<i class='fa fa-user color-blue'></i>"
    elsif feed_type == 1
      "<i class='fa fa-comment color-orange'></i>"
    elsif feed_type == 2
      "<i class='fa fa-map-marker color-blue'></i>"
    elsif feed_type == 3
      "<i class='fa fa-group color-green'></i>"
    elsif feed_type == 4
      "<i class='fa fa-frown-o color-red'></i>"
    end
	end

  def self.winner_icon(result, team)
    if result == 2
      "<i class='fa fa-lg fa-minus color-blue'></i>" 
    elsif result == team
      "<i class='fa fa-lg fa-check color-green'></i>"
    end
  end

	def self.serie_category(category)
		return "Men" if category == 0
		return "Coed" if category == 1
		return "Women" if category == 2
	end

	def self.serie_recurring(recurring)
		return "week" if recurring == 0
		return "month" if recurring == 1
		return "day" if recurring == 2
		return "second week" if recurring == 3
	end

	def self.soccer_field_category(category)
		return "indoor" if category == 0
		return "outdoor" if category == 1
	end

	def self.level(level)
		return "Amateur" if level == 0
		return "Semi Pro" if level == 1
		return "Professional" if level == 2
	end

	def self.user_position(position)
		return "Goalie" if position == 0
		return "Defender" if position == 1
		return "Midfielder" if position == 2
		return "Striker" if position == 3
	end

	def self.game_event_text(event)
		return "scored a goal" if event.category == 0
		return "assisted a goal" if event.category  == 1
		return "received a yellow card" if event.category  == 2
		return "was sent out with a red card" if event.category  == 3
		return "game started" if event.category  == 5
		return "game finished" if event.category  == 6
		return "one day left before kick off" if event.category  == 7
	end

	def self.score_changes(score_changes)
    if score_changes == 0
      "fa-minus color-blue"
    elsif score_changes == 1
      "fa-caret-up color-green"
    else
      "fa-caret-down color-red"
    end
	end

end