class User < ActiveRecord::Base
	devise :database_authenticatable, :registerable, :omniauthable, 
		   :recoverable, :rememberable, :trackable, :validatable, :confirmable
	mount_uploader :picture, PictureUploader

	has_many :presences, dependent: :destroy
	has_many :games, through: :presences
	has_and_belongs_to_many :teams
	belongs_to :city
	has_many :game_chats
	has_many :feeds

	after_create { Feed.welcome(self) }

	def retrieve_user_games
		user_games = []
		games.each do |game|
			user_games << {id: game.id, title: "#{game.time.strftime('%I:%M %p')}", start: game.time, end: game.time+game.duration*60, duration: game.duration, allDay: true}
		end
		user_games
	end

	def rank
		User.valid(self.city_id).index(self) + 1 
	end

	def last_game
		games.where(status: 0).where("time < '#{Time.now}'").order("time DESC").first
	end

	def last_presence
		presences.joins(:game).where("games.status = 0").where("games.time < '#{Time.now}'").where(result: nil).order("time DESC").first
	end

	def played_games
		games.where("games.time < '#{Time.now}'").order("time DESC")
	end

	def games_with_results
		games.where(status: 1).order("time DESC")
	end

  def self.valid(city_id)
    where(city_id: city_id).where.not(first_name: nil).order("points DESC").order("id ASC")
  end

  def wins
  	wins = 0
  	presences.each {|p| wins += 1 if p.team == p.winner }
  	wins
  end

end
