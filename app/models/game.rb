class Game < ActiveRecord::Base
	has_many :presences, dependent: :destroy
	has_many :users, through: :presences
	belongs_to :home, class_name: 'Team'
  belongs_to :away, class_name: 'Team'
  belongs_to :winner, class_name: 'Team'
  belongs_to :soccer_field, class_name: 'SoccerField'
	belongs_to :serie

  def time_difference
    days_difference = TimeDifference.between(self.time, Time.now).in_days.to_i
    days_difference > 0 ? "#{days_difference} day(s)" : "#{TimeDifference.between(self.time, Time.now).in_hours.to_i} hour(s)"
  end

  def self.enter_scores
    Game.where(status: 0).where("games.time < '#{Time.now-7*24*60*60}'").each do |game|
      home_win = game.presences.where(result: 0).count
      away_win = game.presences.where(result: 1).count
      draw = game.presences.where(result: 2).count
      if draw > home_win and draw > away_win
        game.result = 2
        self.enter_results_for_team(game.presences, 2)
      elsif home_win > away_win
        game.result = 0
        self.enter_results_for_team(game.presences, 0)
      else
        game.result = 1
        self.enter_results_for_team(game.presences, 1)
      end
      game.status = 1
      game.save
      Feed.game_results(game)
    end
  end

  def self.enter_results_for_team(presences, result)
    presences.each do |presence|
      user = presence.user
      if presence.fun == 1
        user.fun += 1 
        user.fun_ch = 1
      else
        user.fun_ch = 2
      end
      if result == 2
        user.points += 1
        user.wins_ch = 2
      elsif result == presence.team
        user.points += 3
        user.wins += 1
        user.wins_ch = 1
      else
        user.wins_ch = 2
      end
      user.save
    end
  end

end
