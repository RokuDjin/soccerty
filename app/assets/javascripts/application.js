// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery/jquery-2.0.3.min
//= require jquery-maskedinput/jquery.maskedinput
//= require parsley/parsley.js
//= require select2
//= require fullcalendar/fullcalendar
//= require jquery-ui-1.10.3.custom
//= require jquery.slimscroll
//= require forms-elemets

//= require nvd3/lib/d3.v2
//= require nvd3/nv.d3.custom
//= require nvd3/src/models/scatter
//= require nvd3/src/models/axis
//= require nvd3/src/models/legend
//= require nvd3/src/models/stackedArea
//= require nvd3/src/models/stackedAreaChart
//= require nvd3/src/models/line
//= require nvd3/src/models/pie
//= require nvd3/src/models/pieChartTotal
//= require nvd3/src/models/multiBar
//= require nvd3/src/models/multiBarChart
//= require nvd3/src/models/lineChart
//= require nvd3/stream_layers

//= require backbone/underscore-min
//= require backbone/backbone-min
//= require backbone/backbone.localStorage-min

//= require bootstrap/transition
//= require bootstrap/collapse
//= require bootstrap/alert
//= require bootstrap/tooltip
//= require bootstrap/popover
//= require bootstrap/button
//= require bootstrap/tab
//= require bootstrap/modal
//= require bootstrap/dropdown
//= require bootstrap-datepicker
//= require jquery.bootstrap.wizard.js
//= require bootstrap-select/bootstrap-select

//= require gmap3
//= require jquery.vmap
//= require jquery.vmap.world
//= require jquery.vmap.europe
//= require jquery.vmap.usa
//= require jquery.vmap.australia

//= require_self
