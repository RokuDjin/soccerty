class UsersController < ApplicationController
	before_filter :auth_user, except: [:about_you, :first_update]

	def show
		@user = User.find(params[:id])
	end

	def edit
		@user = current_user
	end

	def update
		@user = User.find(params[:id])
		if @user.update(params[:user].permit(:position, :first_name, :last_name, :gender, :city_id, :level, :picture))
		  redirect_to @user, notice: 'Profile successfully updated.' 
		else
		  render action: :edit
		end
	end

	def orders
		@payments = current_user.payments.paginate(:page => params[:page], :per_page => 20)
	end

	def feeds
		@user = current_user
		@feeds = @user.feeds.order("created_at DESC").paginate(:page => params[:page], :per_page => 20)
	end

	def about_you
		if not user_signed_in?
			redirect_to new_user_session_path, alert: "You need to sign in or sign up before continuing."
		else
			@user = current_user
			render layout: 'semi_raw'
		end
	end

	def first_update
		@user = User.find(params[:id])
		if @user.update(params[:user].permit(:position, :first_name, :last_name, :gender, :city_id, :level, :picture))
		  redirect_to root_path, notice: "Welcome #{@user.first_name} to Soccerty! Start by joining game(s)."
		else
		  render action: :edit
		end
	end

	def ranking
		@users = User.valid(current_user.city_id).paginate(:page => params[:page], :per_page => 20)
	end

end