class TeamsController < ApplicationController
	before_filter :auth_user

	def my
		@team = current_user.teams.last
		if @team.nil?
			redirect_to new_team_path
		else
			redirect_to @team
		end
	end

	def new
		@team = Team.new
	end

	def create
		@team = Team.new(params[:team].permit(:name, :color))
		@team.users << current_user
		@team.captain = current_user
		if @team.save
			redirect_to @team, notice: "Congratulations! You successfuly started a team. You are now the captain."
		else
			render :new
		end
	end

	def show
		@team = Team.find(params[:id])
		@games = Game.where("home_id = #{@team.id} OR away_id = #{@team.id}").order(time: :desc).limit(10)
		@invitation = Invitation.new
	end

	def edit
		@team = Team.find(params[:id])
	end

	def update
		@team = Team.find(params[:id])
		if @team.update(params[:team].permit(:name, :color))
		  redirect_to @team, notice: 'Team successfully updated!' 
		else
		  render action: :edit
		end
	end

	def invitation
		@invitation = Invitation.new(params[:invitation].permit(:email, :team_id))
		if @invitation.save
			redirect_to @invitation.team, notice: 'Player successfully invited!' 
		else
			@team = @invitation.team
			@games = Game.where("home_id = #{@team.id} OR away_id = #{@team.id}").order(time: :desc).limit(10)
			render :show
		end
	end

	def accept
		@team = Team.find(params[:id])
		@team.users << current_user
		if @team.save
			redirect_to my_teams_path, notice: "Congratulations! You successfully joined your new team."
		end
	end

end
