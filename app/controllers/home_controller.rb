class HomeController < ApplicationController
  before_filter :auth_user, except: [:terms, :join]

  def index
    @user = current_user
    @games = @user.games
    @upcoming =  @games.where("games.time > '#{Time.now}'").order("time ASC").limit(5)
    @results =  @games.where(status: 1).order("time DESC").limit(5)
    if User.valid(@user.city_id).count < 5
      @users = User.valid(@user.city_id).limit(5)
    else
      max_offset = User.valid(@user.city_id).count-5 
      @users = User.valid(@user.city_id).offset(@user.rank-1 > max_offset ? max_offset : @user.rank-1).limit(5)
    end
    @next_game = @games.where("games.time > '#{Time.now}'").order("time ASC").first
    @join_suggestions = Serie.where("status < 2").order("start ASC").limit(3)
    finished_games = @user.games_with_results.count.to_f
    unless finished_games == 0
        @win_ratio = (@user.wins/finished_games*100).to_i
        @fun = (@user.fun/finished_games*100).to_i
        @presence = @user.last_presence
    end
    @feeds = current_user.feeds.order("created_at DESC").limit(5)
  end

  def legal
    render :layout => 'raw'
  end

  def join
    if user_signed_in?
      redirect_to series_path(id: params[:id])
    else
      redirect_to new_user_registration_path, notice: "To be able to register as an individual or a team you must first register as a Soccerty User" 
    end
  end

end