class ApplicationController < ActionController::Base

	protect_from_forgery with: :exception

	around_filter :user_time_zone, if: :current_user

	private

	def user_time_zone(&block)
	  Time.use_zone(current_user.city.time_zone, &block)
	end

	protected
	
	before_filter :store_location
	def store_location
	  session[:previous_url] = request.fullpath unless request.fullpath =~ /\/users/
	end

	def after_sign_in_path_for(resource)
	  session[:previous_url] || root_path
	end

	def auth_user
		if not user_signed_in?
			redirect_to new_user_session_path, alert: "You need to sign in or sign up before continuing."
		elsif current_user.first_name.nil? or current_user.last_name.nil?
			redirect_to about_you_users_path
		end
	end

	def after_sign_out_path_for(resource_or_scope)
		new_user_session_path
	end

	def after_sign_up_path_for(resource)
		about_you_users_path
	end

end
