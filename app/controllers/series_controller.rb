class SeriesController < ApplicationController
  before_filter :auth_user

  def index
    @series = Serie.where("status < 2").where("start > '#{Time.now}'").paginate(:page => params[:page], :per_page => 20)
  end

  def show
    @serie = Serie.find(params[:id])
    @games = @serie.retrieve_games
  end

  def new
    @serie = Serie.new
    @serie.build_soccer_field
  end

  def create
    @serie = Serie.new(params[:serie].permit(:game_category, :level, :payment, :organizer_role, :recurring, :game_type, :game_cost, :number_games, :duration, :soccer_field_attributes => [:name, :address, :postal, :city_id, :category]))
    Time.zone = "EST"
    puts Time.zone.parse("2010-01-01 #{params[:serie][:time]}:00")
    @serie.time = Time.zone.parse("2010-01-01 #{params[:serie][:time]}:00")
    @serie.start = Time.zone.parse("#{params[:serie][:start]} 01:00:00")
    @serie.organizer = current_user
    if @serie.save
      redirect_to @serie, notice: "You've successfully setup your serie."
    else
      render :new
    end
  end

  def register_individual
    @serie = Serie.find(params[:id]) 
    @team = @serie.teams.first    
    @team.users << current_user
    @team.captain = current_user if @team.captain.nil?
    if @team.save
      Feed.joined_serie(current_user.id, @serie)
      redirect_to @serie, notice: "Hoorah! You've successfully registered to the serie. Game details will show up before your first game."
    end
  end

  def register_team
    @serie = Serie.find(params[:id])
    if current_user.series.include? @serie
      redirect_to @serie
    else      
       @serie.add_player(current_user)
       redirect_to @serie, notice: "Hoorah! You've successfully registered to the serie. Game details will show up before your first game."
    end
  end

end
