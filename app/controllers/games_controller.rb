class GamesController < ApplicationController
  before_filter :auth_user

  def show
    @game = Game.find(params[:id])
  end

  def send_feedback
    Presence.find(params[:id]).update(result: params[:presence][:result], fun: params[:presence][:fun])
    redirect_to root_path, notice: 'Thank you!' 
  end

  def new_chat
    GameChat.new_game_chat(params[:game_id], current_user.id, params[:team], params[:message])
    respond_to do |format|
      format.json { render :json => true }
    end
  end

  def index
    @calendar_games = current_user.retrieve_user_games
  end

  def map
    @soccer_field = SoccerField.find(params[:id])
  end

  def upcoming 
    @upcoming = current_user.games.where("games.time > '#{Time.now}'").order("time ASC").paginate(:page => params[:page], :per_page => 20)
  end

  def results
    @results = current_user.games.where(status: 1).order("time DESC").paginate(:page => params[:page], :per_page => 20)
  end
  
end
