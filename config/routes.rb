Soccerty::Application.routes.draw do

  authenticated :user do
      root to: "home#index", as: :authenticated_root
  end

  root to: "visitors#index"
  get :join, to: "home#join"
  get :legal, to: "home#legal"
  get :about, to: "home#about"
  get :faq, to: "home#faq"

  devise_for :users

  resources :users do
    collection do
      get :orders
      get :feeds
      get :about_you
      get :ranking
      put :first_update
    end
  end

  resources :games do 
    collection do 
       get :new_chat
       get :map
       get :upcoming
       get :results
    end
  end

  resources :teams do
    collection do
      get :my
      post :invitation
      get :accept
    end
  end

  resources :series do 
    collection do 
      post :register_team
      post :register_individual
    end
  end

  resources :soccer_fields do
    collection do 
      get :retrieve
    end
  end

end
