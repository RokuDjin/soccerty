# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140814223907) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "cities", force: true do |t|
    t.string "name"
    t.string "state"
    t.string "country"
    t.string "time_zone"
  end

  create_table "feeds", force: true do |t|
    t.text     "content"
    t.integer  "status",     default: 0
    t.integer  "feed_type"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "games", force: true do |t|
    t.integer  "number"
    t.integer  "home_id"
    t.integer  "away_id"
    t.datetime "time"
    t.integer  "duration"
    t.integer  "soccer_field_id"
    t.integer  "serie_id"
    t.integer  "status",          default: 0
    t.integer  "winner_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "invitations", force: true do |t|
    t.string   "email"
    t.integer  "team_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "presences", force: true do |t|
    t.integer  "user_id"
    t.integer  "game_id"
    t.integer  "team"
    t.integer  "position"
    t.integer  "winner_id"
    t.integer  "goals"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "series", force: true do |t|
    t.integer  "game_type",        default: 5
    t.integer  "game_category",    default: 0
    t.integer  "level",            default: 0
    t.integer  "number_games"
    t.integer  "soccer_field_id"
    t.datetime "start"
    t.integer  "duration"
    t.integer  "recurring",        default: 0
    t.integer  "status",           default: 0
    t.integer  "team_price"
    t.integer  "individual_price"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "soccer_fields", force: true do |t|
    t.string   "name"
    t.string   "address"
    t.integer  "city_id"
    t.string   "postal"
    t.integer  "category",   default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "team_chats", force: true do |t|
    t.integer  "user_id"
    t.integer  "team_id"
    t.text     "message"
    t.datetime "time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "teams", force: true do |t|
    t.string   "name"
    t.string   "color"
    t.integer  "serie_id"
    t.integer  "captain_id"
    t.integer  "max",        default: 10
    t.integer  "team_type",  default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "teams_users", id: false, force: true do |t|
    t.integer "user_id"
    t.integer "team_id"
    t.integer "captain_id"
  end

  create_table "users", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.integer  "gender",                 default: 0
    t.integer  "city_id",                default: 1
    t.integer  "position",               default: 0
    t.integer  "level",                  default: 0
    t.string   "picture"
    t.integer  "points",                 default: 0
    t.integer  "role",                   default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
