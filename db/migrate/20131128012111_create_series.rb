class CreateSeries < ActiveRecord::Migration
  	def change
    	create_table :series do |t|
			t.integer :game_type, default: 5
			t.integer :game_category, default: 0
			t.integer :level, default: 0
			t.integer :number_games
			t.integer :soccer_field_id
			t.datetime :start
			t.integer :duration
			t.integer :status, default: 0
			t.integer :team_price
			t.integer :individual_price
			t.timestamps
    	end
  	end
end
