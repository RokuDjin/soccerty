class CreateTeamChats < ActiveRecord::Migration
  def change
    create_table :team_chats do |t|
			t.integer :user_id
      t.integer :team_id
      t.text :message
      t.datetime :time
      t.timestamps
    end
  end
end
