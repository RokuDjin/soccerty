class CreateInvitations < ActiveRecord::Migration
  def change
    create_table :invitations do |t|
    	t.string :email
    	t.belongs_to :team
      t.timestamps
    end
  end
end
