class CreateTeams < ActiveRecord::Migration
  def change
    create_table :teams do |t|
      t.string :name
      t.string :color
      t.integer :serie_id
      t.integer :captain_id
      t.integer :max, default: 10
      t.integer :team_type, default: 0
      t.timestamps
    end
  end
end
