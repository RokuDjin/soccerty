class CreateUsers < ActiveRecord::Migration
	def change
		create_table :users do |t|
			t.string :first_name
			t.string :last_name
			t.integer :gender, default: 0
			t.integer :city_id, default: 1
			t.integer :position, default: 0
			t.integer :level, default: 0
			t.string :picture
			t.integer :points, default: 0
			t.integer :role, default: 0
			t.timestamps
		end
	end
end
