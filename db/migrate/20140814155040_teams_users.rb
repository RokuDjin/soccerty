class TeamsUsers < ActiveRecord::Migration

	def change
    create_table :teams_users, id: false do |t|
      t.belongs_to :user
      t.belongs_to :team
      t.integer :captain_id
    end
  end
end
