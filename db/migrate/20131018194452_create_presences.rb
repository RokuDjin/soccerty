class CreatePresences < ActiveRecord::Migration
	def change
		create_table :presences do |t|
			t.integer :user_id
			t.integer :game_id
			t.integer :team
			t.integer :position
			t.integer :winner_id
			t.integer :goals
			t.timestamps
		end
	end
end
