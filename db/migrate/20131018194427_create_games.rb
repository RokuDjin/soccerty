class CreateGames < ActiveRecord::Migration
	def change
		create_table :games do |t|
			t.integer :number
			t.integer :home_id
			t.integer :away_id
			t.datetime :time
			t.integer :duration
			t.integer :soccer_field_id
			t.integer :serie_id
			t.integer :status, default: 0
			t.integer :winner_id
			t.timestamps
		end
	end
end
