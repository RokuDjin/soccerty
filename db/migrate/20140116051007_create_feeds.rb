class CreateFeeds < ActiveRecord::Migration
  def change
    create_table :feeds do |t|
      t.text :content
      t.integer :status, default: 0
      t.integer :feed_type
      t.integer :user_id
      t.timestamps
    end
  end
end
