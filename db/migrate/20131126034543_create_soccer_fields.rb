class CreateSoccerFields < ActiveRecord::Migration
	def change
		create_table :soccer_fields do |t|
			t.string :name
			t.string :address
			t.integer :city_id
			t.string :postal
			t.integer :category, default: 0
			t.timestamps
		end
	end
end
