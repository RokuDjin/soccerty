

City.find_or_create_by_name(name: "Vancouver", state: "BC", country: "Canada", time_zone: "Pacific Time (US & Canada)")

User.find_or_create_by_email(email: "maher.manoubi@gmail.com", first_name: "Maher", last_name: "Manoubi", password: "password", password_confirmation: "password", confirmed_at: Time.now, city_id: 1, gender: 0, position: 3, role: 1, level: 1)

SoccerField.find_or_create_by_name(name: "Super Dome", address: "1660 Bearbrook Rd", city_id: 1, postal: "K1B 1C4", category: 0) 
SoccerField.find_or_create_by_name(name: "Plouf Park", address: "141 Preston St", city_id: 1, postal: "K1R 7P4", category: 1) 


# player1 = User.find_or_create_by_email(email: "player1@soccerty.com", first_name: "Samuel", last_name: "King", password: "password", password_confirmation: "password", confirmed_at: Time.now, city_id: 1, gender: 0, position: 3, level: 1)
# player2 = User.find_or_create_by_email(email: "player2@soccerty.com", first_name: "Roger", last_name: "Lewis", password: "password", password_confirmation: "password", confirmed_at: Time.now, city_id: 1, gender: 0, position: 1, level: 1)
# player3 = User.find_or_create_by_email(email: "player3@soccerty.com", first_name: "Robert", last_name: "Bowden", password: "password", password_confirmation: "password", confirmed_at: Time.now, city_id: 1, gender: 0, position: 0, level: 1)
# player4 = User.find_or_create_by_email(email: "player4@soccerty.com", first_name: "William", last_name: "Pena", password: "password", password_confirmation: "password", confirmed_at: Time.now, city_id: 1, gender: 1, position: 2, level: 1)
# player5 = User.find_or_create_by_email(email: "player5@soccerty.com", first_name: "Brad", last_name: "Bechtel", password: "password", password_confirmation: "password", confirmed_at: Time.now, city_id: 1, gender: 0, position: 3, level: 1)
# player6 = User.find_or_create_by_email(email: "player6@soccerty.com", first_name: "Stefan", last_name: "Bowling", password: "password", password_confirmation: "password", confirmed_at: Time.now, city_id: 1, gender: 0, position: 0, level: 1)
# player7 = User.find_or_create_by_email(email: "player7@soccerty.com", first_name: "Jose", last_name: "Luna", password: "password", password_confirmation: "password", confirmed_at: Time.now, city_id: 1, gender: 0, position: 3, level: 1)
# player8 = User.find_or_create_by_email(email: "player8@soccerty.com", first_name: "Manuel", last_name: "Clement", password: "password", password_confirmation: "password", confirmed_at: Time.now, city_id: 1, gender: 0, position: 1, level: 1)
# player9 = User.find_or_create_by_email(email: "player9@soccerty.com", first_name: "William", last_name: "Wilkens", password: "password", password_confirmation: "password", confirmed_at: Time.now, city_id: 1, gender: 0, position: 2, level: 1)
# player10 = User.find_or_create_by_email(email: "player10@soccerty.com", first_name: "Barbara", last_name: "Vue", password: "password", password_confirmation: "password", confirmed_at: Time.now, city_id: 1, gender: 1, position: 2, level: 1)
# player11 = User.find_or_create_by_email(email: "player11@soccerty.com", first_name: "Christine", last_name: "Cottman", password: "password", password_confirmation: "password", confirmed_at: Time.now, city_id: 1, gender: 1, position: 3, level: 1)

# s1 = Serie.create(game_type: 7, game_category: 1, level: 1, number_games: 10, soccer_field_id: 1, start: Time.now+5*24*60*60, duration: 90, team_price: 300, individual_price: 90)


# s3 = Serie.create(game_type: 5, game_category: 2, level: 2, number_games: 10, organizer_id: 1, soccer_field_id: 1, start: Time.now-110*24*60*60, time: Time.now.change(minutes: 0, :month => 1, :day => 1, :year => 2000), duration: 120, recurring: 0, game_cost: 8)
# s3.add_player(player1)
# s3.add_player(player2)
# s3.add_player(player3)
# s3.add_player(player4)
# s3.add_player(player5)
# s3.add_player(player6)
# s3.add_player(player7)
# s3.add_player(player8)
# s3.add_player(player9)
# s3.add_player(player10)
# s3.add_player(player11)
# s3.setup
# s3.games.each do |game|
#   game.presences.each do |presence|
#     presence.update(result: [0, 1, 2].sample, fun: [0, 1].sample)
#   end
# end

# s4 = Serie.create(game_type: 7, game_category: 2, level: 1, number_games: 4, organizer_id: 1, soccer_field_id: 1, start: Time.now+30*24*60*60, time: Time.now.change(minutes: 0, :month => 1, :day => 1, :year => 2000), duration: 120, recurring: 0, game_cost: 12)
# s4.add_player(player6)
# s4.add_player(player7)
# s4.add_player(player8)

# s6 = Serie.create(game_type: 5, game_category: 0, level: 2, number_games: 5, organizer_id: 1, soccer_field_id: 2, start: Time.now-20*24*60*60, time: Time.now.change(minutes: 0, :month => 1, :day => 1, :year => 2000), duration: 90, recurring: 0, game_cost: 5)
# s6.add_player(player1)
# s6.add_player(player2)
# s6.add_player(player3)
# s6.add_player(player4)
# s6.add_player(player5)
# s6.add_player(player6)
# s6.add_player(player7)
# s6.add_player(player8)
# s6.add_player(player9)
# s6.add_player(player10)
# s6.add_player(player11)
# s6.setup

# Game.enter_scores
