task :setup_games => :environment do
  puts "Setting up game(s)"
  Serie.setup_games
  puts "Done setting up game(s)."
end

task :enter_scores => :environment do
  Game.enter_scores
end